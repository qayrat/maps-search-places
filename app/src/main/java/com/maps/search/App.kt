package com.maps.search

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import androidx.preference.PreferenceManager
import com.maps.search.data.pref.IPreference
import com.maps.search.data.pref.Preference
import timber.log.Timber

class App : Application() {
    companion object {
        @SuppressLint("StaticFieldLeak")
        var context: Context? = null
        val pref: IPreference by lazy(LazyThreadSafetyMode.NONE) {
            Preference(PreferenceManager.getDefaultSharedPreferences(context!!))
        }
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        context = this
    }
}