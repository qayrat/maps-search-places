package com.maps.search.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.maps.search.data.model.direction.DirectionsLiveDataModel
import com.maps.search.data.model.maps.MapGeoCodeLiveDataModel
import com.maps.search.data.model.searchQuery.SearchResultLiveDataModel
import com.maps.search.data.network.apiClientGeoCoder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MapViewModel : ViewModel() {
    private val _queryList = MutableLiveData<SearchResultLiveDataModel>()
    private val _placeList = MutableLiveData<MapGeoCodeLiveDataModel>()
    private val _placeList2 = MutableLiveData<MapGeoCodeLiveDataModel>()
    private val _directionMap = MutableLiveData<DirectionsLiveDataModel>()
    val queryList: LiveData<SearchResultLiveDataModel> = _queryList
    val placeList: LiveData<MapGeoCodeLiveDataModel> = _placeList
    val placeList2: LiveData<MapGeoCodeLiveDataModel> = _placeList2
    val directionMap: LiveData<DirectionsLiveDataModel> = _directionMap

    private val disposable = CompositeDisposable()

    fun searchQueryRequest(query: String, lang: String, key: String) {
        disposable.add(
            apiClientGeoCoder.queryAutocomplete(query, lang, key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { _queryList.value = SearchResultLiveDataModel(it.predictions, null) },
                    { _queryList.value = SearchResultLiveDataModel(null, it) })
        )
    }

    fun geoCoding(query: String, key: String) {
        disposable.add(
            apiClientGeoCoder.getGeoCode(query, key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { _placeList.value = MapGeoCodeLiveDataModel(it.results, null) },
                    { _placeList.value = MapGeoCodeLiveDataModel(null, it) })
        )
    }

    fun geoCodingMainSearch(query: String, key: String) {
        disposable.add(
            apiClientGeoCoder.getGeoCode(query, key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { _placeList2.value = MapGeoCodeLiveDataModel(it.results, null) },
                    { _placeList2.value = MapGeoCodeLiveDataModel(null, it) })
        )
    }

    fun getDirections(destination: String, mode: String, origin: String, key: String) {
        disposable.add(
            apiClientGeoCoder.getDirections(destination, origin, key)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { _directionMap.value = DirectionsLiveDataModel(it, null) },
                    { _directionMap.value = DirectionsLiveDataModel(null, it) })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}