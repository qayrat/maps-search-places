package com.maps.search.ui.map.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.maps.search.R
import com.maps.search.data.model.searchQuery.SearchResult
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.search_result_layout.*
import timber.log.Timber

class SearchResultAdapter(
    context: Fragment
) : RecyclerView.Adapter<SearchResultAdapter.ViewHolder>() {
    private val layoutInflater = LayoutInflater.from(context.requireContext())
    private val listener = context as OnItemClickListener
    private var list: List<SearchResult> = ArrayList()
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = layoutInflater.inflate(R.layout.search_result_layout, parent, false)
        return ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bindData(list[position])

    override fun getItemCount(): Int = list.size

    fun setData(data: List<SearchResult>) {
        list = data
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View, listener: OnItemClickListener) :
        RecyclerView.ViewHolder(itemView), LayoutContainer {
        override val containerView: View
            get() = itemView

        private lateinit var itemData: SearchResult

        init {
            textView.setOnClickListener {
                listener.onClick(itemData)
            }
        }

        fun bindData(data: SearchResult) {
            itemData = data
            Timber.d(data.description)
            textView.text = data.description
        }
    }

    interface OnItemClickListener {
        fun onClick(data: SearchResult)
    }
}