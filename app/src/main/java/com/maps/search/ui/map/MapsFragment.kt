package com.maps.search.ui.map

import android.Manifest
import android.content.Context
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.maps.search.App
import com.maps.search.R
import com.maps.search.data.model.maps.GeoCodeResultModel
import com.maps.search.extensions.OnDirectionClickListener
import com.maps.search.ui.MapViewModel
import com.maps.search.ui.searchDialog.SearchDialogFragment
import kotlinx.android.synthetic.main.maps_fragment.*
import timber.log.Timber
import java.util.*

class MapsFragment : Fragment(R.layout.maps_fragment), OnMapReadyCallback,
    GoogleMap.OnMyLocationClickListener,
    GoogleMap.OnMyLocationButtonClickListener {
    private val viewModel: MapViewModel by activityViewModels()
    private lateinit var mMap: GoogleMap
    private lateinit var mapView: View
    private var locationPermissionGranted = false
    private var listener: OnDirectionClickListener? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapView = mapFragment.requireView()
        mapFragment.getMapAsync(this)
        changeMapButton()

        viewModel.placeList2.observe(viewLifecycleOwner, Observer {
            if (it.repos != null) {
                addMarker(it.repos)
            } else {
                logException(it.error)
            }
        })

        iv_direction.setOnClickListener {
            listener?.onClick()
        }

        tv_search.setOnClickListener {
            openDialog()
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // location permission check
        getLocationPermission()
        mMap.setOnMyLocationButtonClickListener(this)
        mMap.setOnMyLocationClickListener(this)
        mMap.uiSettings.isCompassEnabled = true

        // Add move the camera in Tashkent
        val tashkent = LatLng(41.3082039, 69.2736238)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(tashkent))
    }

    private fun openDialog() {
        val dialog: SearchDialogFragment = SearchDialogFragment.newInstance("mapsFragment")
        dialog.show(childFragmentManager, "searchDialogFragment")
    }

    private fun addMarker(data: ArrayList<GeoCodeResultModel>) {
        // Add move the camera in Tashkent
        val lat = data[0].geometry.location.lat
        val lng = data[0].geometry.location.lng
        val location = LatLng(lat, lng)
        mMap.addMarker(MarkerOptions().position(location).snippet(data[0].formattedAddress))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 13f))
        search_text.text = data[0].formattedAddress
    }

    private fun logException(error: Throwable?) {
        Toast.makeText(requireContext(), error?.message, Toast.LENGTH_SHORT).show()
    }

    private fun changeMapButton() {
        // location button
        var view =
            (mapView.findViewById<View>("1".toInt()).parent as View).findViewById<View>("2".toInt())
        var layoutParams: RelativeLayout.LayoutParams =
            view.layoutParams as RelativeLayout.LayoutParams
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        layoutParams.setMargins(0, 0, 10, 300)

        // compass button
        view =
            (mapView.findViewById<View>("1".toInt()).parent as View).findViewById<View>("5".toInt())
        layoutParams = view.layoutParams as RelativeLayout.LayoutParams
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE)
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0)
        layoutParams.setMargins(10, 300, 0, 0)
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true
            updateLocationUI()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    private fun updateLocationUI() {
        try {
            if (locationPermissionGranted) {
                mMap.isMyLocationEnabled = true
                mMap.uiSettings.isMyLocationButtonEnabled = true
            } else {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings.isMyLocationButtonEnabled = false
            }
        } catch (e: SecurityException) {
            e.printStackTrace()
        }
    }

    private fun createLocationRequest() {
        val locationRequest = LocationRequest.create().apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
        val client: SettingsClient = LocationServices.getSettingsClient(requireContext())
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener {
        }
        task.addOnFailureListener {
            if (it is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    it.startResolutionForResult(
                        requireActivity(),
                        REQUEST_CHECK_SETTINGS
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    locationPermissionGranted = true
                }
            }
        }
        updateLocationUI()
    }

    override fun onMyLocationClick(p0: Location) {
        Timber.d("current location: lat=${p0.latitude} long=${p0.longitude}")
    }

    override fun onMyLocationButtonClick(): Boolean {
        Timber.d("onMyLocationButtonClick")
        createLocationRequest()
        return false
    }

    companion object {
        private const val REQUEST_CHECK_SETTINGS = 101
        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnDirectionClickListener) {
            listener = context
        }
    }
}