package com.maps.search.ui.searchDialog

import android.app.Dialog
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.maps.search.R
import com.maps.search.data.model.searchQuery.SearchResult
import com.maps.search.ui.MapViewModel
import com.maps.search.ui.map.adapter.SearchResultAdapter
import kotlinx.android.synthetic.main.search_dialog_fragment.*
import timber.log.Timber

class SearchDialogFragment : BottomSheetDialogFragment(), SearchResultAdapter.OnItemClickListener {
    private val viewModel: MapViewModel by activityViewModels()
    private var searchResultAdapter: SearchResultAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.search_dialog_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchView.onActionViewExpanded()
        Timber.d("key: $key")
        rv_results.layoutManager =
            StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        searchResultAdapter = SearchResultAdapter(this)
        rv_results.adapter = searchResultAdapter
        viewModel.queryList.observe(viewLifecycleOwner, Observer {
            if (it.repos != null) {
                searchResultAdapter?.setData(it.repos)
            } else {
                logException(it.error)
            }
        })
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                Timber.d("query: $newText")
                searchQuery(newText)
                return false
            }
        })
        searchView.setOnClickListener {
            searchView.isIconified = false
        }
    }

    private fun searchQuery(text: String) {
        Timber.d("searchQuery")
        viewModel.searchQueryRequest(text, "ru", getString(R.string.google_maps_key))
    }

    private fun logException(error: Throwable?) {
        Toast.makeText(requireContext(), error?.message, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener {
            val bottomSheetDialog: BottomSheetDialog = it as BottomSheetDialog
            setupFullHeight(bottomSheetDialog)
        }
        return dialog
    }

    private fun setupFullHeight(bottomSheetDialog: BottomSheetDialog) {
        val bottomSheet: FrameLayout =
            bottomSheetDialog.findViewById<FrameLayout>(R.id.design_bottom_sheet)!!
        val behavior = BottomSheetBehavior.from(bottomSheet)
        val layoutParams: ViewGroup.LayoutParams = bottomSheet.layoutParams
        val windowHeight = getWindowHeight()
        layoutParams.height = windowHeight
        bottomSheet.layoutParams = layoutParams
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
    }

    private fun getWindowHeight(): Int {
        val outMetrics = DisplayMetrics()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
            val display = requireActivity().display
            display?.getRealMetrics(outMetrics)
        } else {
            @Suppress("DEPRECATION")
            val display = requireActivity().windowManager.defaultDisplay
            @Suppress("DEPRECATION")
            display.getMetrics(outMetrics)
        }
        return outMetrics.heightPixels
    }

    companion object {
        private var key = ""
        fun newInstance(key: String): SearchDialogFragment {
            this.key = key
            return SearchDialogFragment()
        }
    }

    override fun onClick(data: SearchResult) {
        if (key == "mapsFragment") {
            viewModel.geoCodingMainSearch(data.description, getString(R.string.google_maps_key))
        } else {
            viewModel.geoCoding(data.description, getString(R.string.google_maps_key))
        }
        searchView.isIconified = true
        dismiss()
    }

    override fun onStop() {
        super.onStop()
        searchView.clearFocus()
    }
}