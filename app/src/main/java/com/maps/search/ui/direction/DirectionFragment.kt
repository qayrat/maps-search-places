package com.maps.search.ui.direction

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.PolyUtil
import com.maps.search.App
import com.maps.search.R
import com.maps.search.data.model.direction.Route
import com.maps.search.ui.MapViewModel
import com.maps.search.ui.searchDialog.SearchDialogFragment
import kotlinx.android.synthetic.main.direction_fragment.*
import timber.log.Timber
import java.util.ArrayList

class DirectionFragment : Fragment(R.layout.direction_fragment), OnMapReadyCallback {
    private val viewModel: MapViewModel by activityViewModels()
    private lateinit var mMap: GoogleMap

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        viewModel.placeList.observe(viewLifecycleOwner, Observer {
            if (it.repos != null) {
                setText(it.repos[0].formattedAddress)
            } else {
                logException(it.error)
            }
        })
        viewModel.directionMap.observe(viewLifecycleOwner, Observer {
            if (it.repos != null) {
                addPolyline(it.repos.routes)
            } else {
                logException(it.error)
            }
        })
        tv_origin.setOnClickListener {
            openDialog("origin")
        }
        tv_destination.setOnClickListener {
            openDialog("destination")
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }

    private fun addPolyline(routes: ArrayList<Route>) {
        mMap.clear()
        Timber.d("routes size: ${routes.size}")
        val lat = routes[0].legs?.get(0)?.endLocation?.lat!!
        val lng = routes[0].legs?.get(0)?.endLocation?.lng!!
        val shape = routes[0].overviewPolyline?.points
        val polyline = PolylineOptions()
            .addAll(PolyUtil.decode(shape))
            .width(12f)
            .color(Color.BLUE)
        mMap.addPolyline(polyline)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 12f))
        mMap.addMarker(MarkerOptions().position(LatLng(lat, lng)))
        val distance: Float = (routes[0].legs?.get(0)?.distance?.value!!).toFloat() / 1000
        val duration: Int = (routes[0].legs?.get(0)?.distance?.value!!) / 60
        tv_direction_title.text = "$duration мин.  $distance км"
    }

    private fun setText(formattedAddress: String) {
        if (App.pref.directionKey == "origin") {
            tv_origin.text = formattedAddress
        } else {
            tv_destination.text = formattedAddress
        }
        if (tv_origin.text.isNotEmpty() && tv_destination.text.isNotEmpty()) {
            val origin: String = tv_origin.text.toString()
            val destination: String = tv_destination.text.toString()
            viewModel.getDirections(
                destination,
                "driving",
                origin,
                getString(R.string.google_maps_key)
            )
        }
    }

    private fun openDialog(key: String) {
        App.pref.directionKey = key
        val dialog: SearchDialogFragment = SearchDialogFragment.newInstance(key)
        dialog.show(childFragmentManager, "searchDialogFragment")
    }

    private fun logException(error: Throwable?) {
        Toast.makeText(requireContext(), error?.message, Toast.LENGTH_SHORT).show()
    }
}