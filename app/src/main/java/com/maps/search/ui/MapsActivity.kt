package com.maps.search.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.add
import androidx.fragment.app.commit
import androidx.fragment.app.replace
import com.maps.search.R
import com.maps.search.extensions.OnDirectionClickListener
import com.maps.search.ui.direction.DirectionFragment
import com.maps.search.ui.map.MapsFragment

class MapsActivity : AppCompatActivity(), OnDirectionClickListener {
    private val viewModel: MapViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                add<MapsFragment>(R.id.container)
            }
        }
    }

    override fun onClick() {
        supportFragmentManager.commit {
            setReorderingAllowed(true)
            replace<DirectionFragment>(R.id.container)
            addToBackStack("directionFragment")
        }
    }

//    private fun openDirectionDialog() {
//        val sheetView = layoutInflater.inflate(R.layout.direction_fragment, null)
//        dialog.setContentView(sheetView)
//        dialog.show()
//    }
//
//    private fun changeMapButton() {
//        // location button
//        var view =
//            (mapView.findViewById<View>("1".toInt()).parent as View).findViewById<View>("2".toInt())
//        var layoutParams: RelativeLayout.LayoutParams =
//            view.layoutParams as RelativeLayout.LayoutParams
//        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
//        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
//        layoutParams.setMargins(0, 0, 10, 300)
//
//        // compass button
//        view =
//            (mapView.findViewById<View>("1".toInt()).parent as View).findViewById<View>("5".toInt())
//        layoutParams = view.layoutParams as RelativeLayout.LayoutParams
//        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE)
//        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0)
//        layoutParams.setMargins(10, 300, 0, 0)
//    }
//
//    /**
//     * Manipulates the map once available.
//     * This callback is triggered when the map is ready to be used.
//     * If Google Play services is not installed on the device, the user will be prompted to install
//     * it inside the SupportMapFragment. This method will only be triggered once the user has
//     * installed Google Play services and returned to the app.
//     */
//    override fun onMapReady(googleMap: GoogleMap) {
//        mMap = googleMap
//
//        // location permission check
//        getLocationPermission()
//        mMap.setOnMyLocationButtonClickListener(this)
//        mMap.setOnMyLocationClickListener(this)
//        mMap.uiSettings.isCompassEnabled = true
//
//        // Add move the camera in Tashkent
//        val tashkent = LatLng(41.3082039, 69.2736238)
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(tashkent))
//    }
//
//    private fun addMarker(data: ArrayList<GeoCodeResultModel>) {
//        // Add move the camera in Tashkent
//        val lat = data[0].geometry.location.lat
//        val lng = data[0].geometry.location.lng
//        val location = LatLng(lat, lng)
//        mMap.addMarker(MarkerOptions().position(location))
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(location))
//    }
//
//    private fun logException(error: Throwable?) {
//        Toast.makeText(this, error?.message, Toast.LENGTH_SHORT).show()
//    }
//
//    private fun searchQuery(text: String) {
//        viewModel.searchQueryRequest(text, "ru", getString(R.string.google_maps_key))
//    }
//
//    private fun getLocationPermission() {
//        if (ContextCompat.checkSelfPermission(
//                this.applicationContext,
//                Manifest.permission.ACCESS_FINE_LOCATION
//            )
//            == PackageManager.PERMISSION_GRANTED
//        ) {
//            locationPermissionGranted = true
//            updateLocationUI()
//        } else {
//            ActivityCompat.requestPermissions(
//                this,
//                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
//                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
//            )
//        }
//    }
//
//    private fun updateLocationUI() {
//        try {
//            if (locationPermissionGranted) {
//                mMap.isMyLocationEnabled = true
//                mMap.uiSettings.isMyLocationButtonEnabled = true
//            } else {
//                mMap.isMyLocationEnabled = false
//                mMap.uiSettings.isMyLocationButtonEnabled = false
//            }
//        } catch (e: SecurityException) {
//            e.printStackTrace()
//        }
//    }
//
//    override fun onMyLocationClick(p0: Location) {
//        Timber.d("current location: lat=${p0.latitude} long=${p0.longitude}")
//    }
//
//    override fun onMyLocationButtonClick(): Boolean {
//        Timber.d("onMyLocationButtonClick")
//        createLocationRequest()
//        return false
//    }
//
//    private fun createLocationRequest() {
//        val locationRequest = LocationRequest.create().apply {
//            interval = 10000
//            fastestInterval = 5000
//            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
//        }
//        val builder = LocationSettingsRequest.Builder()
//            .addLocationRequest(locationRequest)
//        val client: SettingsClient = LocationServices.getSettingsClient(this)
//        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
//        task.addOnSuccessListener {
//        }
//        task.addOnFailureListener {
//            if (it is ResolvableApiException) {
//                // Location settings are not satisfied, but this can be fixed
//                // by showing the user a dialog.
//                try {
//                    // Show the dialog by calling startResolutionForResult(),
//                    // and check the result in onActivityResult().
//                    it.startResolutionForResult(
//                        this,
//                        REQUEST_CHECK_SETTINGS
//                    )
//                } catch (sendEx: IntentSender.SendIntentException) {
//                    // Ignore the error.
//                }
//            }
//        }
//    }
//
//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<out String>,
//        grantResults: IntArray
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        locationPermissionGranted = false
//        when (requestCode) {
//            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.isNotEmpty() &&
//                    grantResults[0] == PackageManager.PERMISSION_GRANTED
//                ) {
//                    locationPermissionGranted = true
//                }
//            }
//        }
//        updateLocationUI()
//    }
//
//    companion object {
//        private const val REQUEST_CHECK_SETTINGS = 101
//        private const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1
//    }
//
//    override fun onClick(data: SearchResult) {
//        viewModel.geoCoding(data.description, getString(R.string.google_maps_key))
//        searchView.isIconified = true
//    }
}