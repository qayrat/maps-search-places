package com.maps.search.data.model.maps

import com.google.gson.annotations.SerializedName

class GeoCodeGeometry(
    val location: LocationModel,
    @SerializedName("location_type")
    val locationType: String,
    val viewport: GeoCodeViewport
)