package com.maps.search.data.model.searchQuery

class MatchedSubstrings(
    val length: Int,
    val offset: Int
)