package com.maps.search.data.model.searchQuery

import com.google.gson.annotations.SerializedName

class SearchResult(
    val description: String,
    @SerializedName("matched_substrings")
    val matchedSubstrings: ArrayList<MatchedSubstrings>,
    @SerializedName("structured_formatting")
    val structuredFormatting: StructuredFormatting,
    val terms: ArrayList<Terms>
)