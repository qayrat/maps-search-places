package com.maps.search.data.model.searchQuery

import com.google.gson.annotations.SerializedName

class StructuredFormatting(
    @SerializedName("main_text")
    val mainText: String,
    @SerializedName("main_text_matched_substrings")
    val mainTextMatchedSubstrings: ArrayList<MatchedSubstrings>,
    @SerializedName("secondary_text")
    val secondaryText: String,
    @SerializedName("secondary_text_matched_substrings")
    val secondaryTextMatchedSubstrings: ArrayList<MatchedSubstrings>
)