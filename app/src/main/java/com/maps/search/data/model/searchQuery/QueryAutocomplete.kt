package com.maps.search.data.model.searchQuery

class QueryAutocomplete(
    val predictions: ArrayList<SearchResult>,
    val status: String
)