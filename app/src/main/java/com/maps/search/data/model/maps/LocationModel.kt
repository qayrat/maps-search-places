package com.maps.search.data.model.maps

class LocationModel(
    val lat: Double,
    val lng: Double
)