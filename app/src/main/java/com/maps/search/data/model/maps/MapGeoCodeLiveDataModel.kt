package com.maps.search.data.model.maps

class MapGeoCodeLiveDataModel(
    val repos: ArrayList<GeoCodeResultModel>? = null,
    val error: Throwable? = null
)