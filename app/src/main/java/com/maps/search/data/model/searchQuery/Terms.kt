package com.maps.search.data.model.searchQuery

class Terms(
    val offset: Int,
    val value: String
)