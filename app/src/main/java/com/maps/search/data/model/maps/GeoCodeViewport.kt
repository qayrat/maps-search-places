package com.maps.search.data.model.maps

class GeoCodeViewport(
    val northeast: LocationModel,
    val southwest: LocationModel
)