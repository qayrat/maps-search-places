package com.maps.search.data.model.direction

import com.google.gson.annotations.SerializedName

class Route(
    val bounds: Bounds?,
    val copyrights: String?,
    val legs: ArrayList<RouteLeg>?,
    @SerializedName("overview_polyline")
    val overviewPolyline: OverviewPolyline?,
    @SerializedName("summary")
    val summary: String?,
    @SerializedName("warnings")
    val warnings: List<Any?>?,
    @SerializedName("waypoint_order")
    val wayPointOrder: List<Any?>?
)