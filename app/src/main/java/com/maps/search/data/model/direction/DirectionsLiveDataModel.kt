package com.maps.search.data.model.direction

import com.maps.search.data.model.searchQuery.SearchResult

class DirectionsLiveDataModel(
    val repos: DirectionMapResponse? = null,
    val error: Throwable? = null
)