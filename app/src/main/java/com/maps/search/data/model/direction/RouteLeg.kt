package com.maps.search.data.model.direction

import com.google.gson.annotations.SerializedName
import com.maps.search.data.model.maps.LocationModel

class RouteLeg(
    @SerializedName("distance")
    val distance: Distance?,
    @SerializedName("duration")
    val duration: Duration?,
    @SerializedName("end_address")
    val endAddress: String?,
    @SerializedName("end_location")
    val endLocation: LocationModel?,
    @SerializedName("start_address")
    val startAddress: String?,
    @SerializedName("start_location")
    val startLocation: LocationModel?,
    @SerializedName("steps")
    val steps: ArrayList<Step>?,
    @SerializedName("traffic_speed_entry")
    val trafficSpeedEntry: ArrayList<Any>?,
    @SerializedName("via_waypoint")
    val viaWayPoint: ArrayList<Any>?
)