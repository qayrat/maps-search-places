package com.maps.search.data.model.direction

import com.google.gson.annotations.SerializedName

class GeoCodedWayPoints(
    @SerializedName("geocoder_status")
    val geoCoderStatus: String,
    @SerializedName("place_id")
    val placeId: String,
    val types: ArrayList<String>,
    @SerializedName("partial_match")
    val partialMatch: Boolean
)