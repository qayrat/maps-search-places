package com.maps.search.data.model.maps

class MapGeoCodeResponse(
    val results: ArrayList<GeoCodeResultModel>
)