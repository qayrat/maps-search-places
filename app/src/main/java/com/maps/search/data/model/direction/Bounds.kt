package com.maps.search.data.model.direction

import com.maps.search.data.model.maps.LocationModel

class Bounds(
    val northeast: LocationModel,
    val southwest: LocationModel
)