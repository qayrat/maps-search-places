package com.maps.search.data.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

const val GOOGLEAPIS_URL = "https://maps.googleapis.com/"

val apiClientGeoCoder: ApiGeoCoder = Retrofit.Builder().client(
    OkHttpClient().newBuilder().build()
)
    .baseUrl(GOOGLEAPIS_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    .build().create(ApiGeoCoder::class.java)