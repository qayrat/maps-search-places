package com.maps.search.data.model.maps

import com.google.gson.annotations.SerializedName
import com.maps.search.data.model.maps.GeoCodeAddressComponentModel
import com.maps.search.data.model.maps.GeoCodeGeometry

class GeoCodeResultModel(
    @SerializedName("address_components")
    val addressComponents: ArrayList<GeoCodeAddressComponentModel>,
    @SerializedName("formatted_address")
    val formattedAddress: String,
    val geometry: GeoCodeGeometry,
    @SerializedName("place_id")
    val placeId: String,
    val types: ArrayList<String>
)