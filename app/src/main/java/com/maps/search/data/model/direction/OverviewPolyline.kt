package com.maps.search.data.model.direction

import com.google.gson.annotations.SerializedName

class OverviewPolyline(
    @SerializedName("points")
    val points: String?
)