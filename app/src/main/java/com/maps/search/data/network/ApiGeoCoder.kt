package com.maps.search.data.network

import com.maps.search.data.model.direction.DirectionMapResponse
import com.maps.search.data.model.maps.MapGeoCodeResponse
import com.maps.search.data.model.searchQuery.QueryAutocomplete
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiGeoCoder {
    @GET("maps/api/geocode/json")
    fun getGeoCode(
        @Query("address") address: String,
        @Query("key") key: String
    ): Single<MapGeoCodeResponse>

    @GET("maps/api/place/queryautocomplete/json")
    fun queryAutocomplete(
        @Query("input") input: String,
        @Query("language") language: String,
        @Query("key") key: String
    ): Single<QueryAutocomplete>

    @GET("maps/api/directions/json")
    fun getDirections(
        @Query("destination") destination: String,
        @Query("origin") origin: String,
        @Query("key") key: String
    ): Single<DirectionMapResponse>
}