package com.maps.search.data.model.direction

import com.google.gson.annotations.SerializedName

class Duration(
    @SerializedName("text")
    val text: String?,
    @SerializedName("value")
    val value: Int?
)