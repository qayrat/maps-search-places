package com.maps.search.data.pref

import android.content.SharedPreferences

private const val DIRECTION_KEY = "DIRECTION_KEY"

class Preference(pref: SharedPreferences): IPreference {
    override var directionKey: String by StringPreference(pref, DIRECTION_KEY, "")
}