package com.maps.search.data.model.searchQuery

class SearchResultLiveDataModel(
    val repos: ArrayList<SearchResult>? = null,
    val error: Throwable? = null
)