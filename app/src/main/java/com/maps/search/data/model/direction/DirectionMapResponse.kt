package com.maps.search.data.model.direction

import com.google.gson.annotations.SerializedName

class DirectionMapResponse(
    @SerializedName("geocoded_waypoints")
    val geoCodedWayPoints: ArrayList<GeoCodedWayPoints>,
    val routes: ArrayList<Route>,
    val status: String
)