package com.maps.search.data.model.direction

import com.google.gson.annotations.SerializedName
import com.maps.search.data.model.maps.LocationModel

class Step(
    @SerializedName("distance")
    val distance: Distance?,
    @SerializedName("duration")
    val duration: Duration?,
    @SerializedName("end_location")
    val endLocation: LocationModel?,
    @SerializedName("html_instructions")
    val htmlInstructions: String?,
    @SerializedName("maneuver")
    val maneuver: String?,
    @SerializedName("polyline")
    val polyline: Polyline?,
    @SerializedName("start_location")
    val startLocation: LocationModel?,
    @SerializedName("travel_mode")
    val travelMode: String?
)