package com.maps.search.data.model.maps

import com.google.gson.annotations.SerializedName

class GeoCodeAddressComponentModel(
    @SerializedName("long_name")
    val longName: String,
    @SerializedName("short_name")
    val shortName: String,
    val types: ArrayList<String>
)