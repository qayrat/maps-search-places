package com.maps.search.data.model.direction

import com.google.gson.annotations.SerializedName

class Polyline(
    @SerializedName("points")
    val points: String?
)