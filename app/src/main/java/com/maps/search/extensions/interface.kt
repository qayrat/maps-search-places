package com.maps.search.extensions

interface OnDirectionClickListener {
    fun onClick()
}